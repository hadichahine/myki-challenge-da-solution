**Notes**

- Project runs with the same instructions given below.
- Use `npm run cleardb` to clear the realm database.
- Refactoring beyond this point is uncertain (rather dangerous). An additional influx of requirements might push further refactoring.
- If you're not sure about my design implementation skills, I would accept some small design document to be implemented. Trying to design by my own is a painstaking effort.

**Welcome to the MYKI Desktop App Challenge!**

In this challenge we are expecting the following:

- Ability to run the demo project on you machine
- Ability to make changes on the code following the requirements
- Research (if needed) for the best most secure way of implementing the requirements
- Preparation before the technical interview, so during the call, we can discuss issues and enhancements

This project is a basic electron app, that will run on both Windows and Mac machines.
For running the project you need the following:

- Install the required node_modules : `npm install`
- Run the Electron App locally on localhost:3000 : `npm run dev`

The requirements are the following:

1. A Lock Screen for the application (as shown in image ‘LockScreen.png’) where the user needs to create a 6-digit pin code on the first launch, and then use it each time the app restarts.
2. A main page where the user can navigate through his passwords (as shown in the image ‘MainScreen.png’)
3. Ability add a password item and save it the database, and instantly showing the newly created item in the items list.(as shown in the image ‘CreatePassword.png’
4. Creation of a Password Generator component when creating the password (as shown in the image ‘PasswordGenerator.png)
5. Add a section in the settings section that can indicate if the machine running the app is a Windows or Mac machine.

BONUS:

1. Add CRUD operation for the password (edit and delete of an item)
2. Add badge count for both Windows and Mac, that can show the number of items(passwords) stored
3. Add a menu with the option to restart the application
4. Add search mechanism for the passwords

Ideally, we advice you using React for the UI, Realm for databases and Redux Store.

You can find attached the images.

This challenge is presented to you before the technical call, so you can prepare it and send us all the questions you have, so that during our 1.5 hour call we can run it together or fix/enhance any issue encountered during the development.

Good Luck !

![Lock Screen](./images/LockScreen.png)
![Main Screen](./images/MainScreen.png)
![Create Password](./images/CreatePassword.png)
![Password Generator](./images/PasswordGenerator.png)
