import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "auth",
  initialState: {
    encryptionKey: "",
    hasPasscode: false,
    locked: true,
  },
  reducers: {
    initAuth(state, { payload: hasPasscode }) {
      state.hasPasscode = hasPasscode;
    },
    unlock(state, { payload: { encryptionKey } }) {
      state.encryptionKey = encryptionKey;
      state.locked = false;
    },
  },
});

export const { initAuth, unlock } = slice.actions;

export default slice.reducer;
