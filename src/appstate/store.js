import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import auth from "./auth";
import passwords from "./passwords";

const attachGlobalState = (storeAPI) => (next) => (action) => {
  return next({
    type: action.type,
    payload: action.payload,
    global: storeAPI.getState(),
  });
};

const store = configureStore({
  reducer: combineReducers({
    auth: auth,
    passwords: passwords,
  }),
  middleware: [attachGlobalState],
});

export default store;
