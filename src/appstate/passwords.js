import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "passwords",
  initialState: [],
  reducers: {
    loadInitialPasswords(state, { payload: { passwords } }) {
      state.push(...passwords);
    },
    addPassword(state, { payload: { nickname, username, password, host } }) {
      state.push({ nickname, username, password, host });
    },
  },
});

export const { loadInitialPasswords, addPassword: addPasswordAction } =
  slice.actions;

export default slice.reducer;
