import React, { useEffect, useState } from "react";
import { loadDB, getDB } from "./database";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "./pages/Home";
import { useSelector } from "react-redux";
import { initAuth } from "./appstate/auth";

import store from "./appstate/store";
import SettingsPage from "./pages/Settings";
import PasswordsPage from "./pages/Passwords";
import ChangePinPage from "./pages/ChangePinPage";
import UnlockPage from "./pages/UnlockPage";
import styled from "styled-components";

const Loading = styled.div`
  font-size: 20px;
  color: white;
`;

export default function App() {
  const isLocked = useSelector((state) => state.auth.locked);

  const hasPasscode = useSelector((state) => state.auth.hasPasscode);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async function () {
      await loadDB();
      const db = getDB();
      const hasPasscode = db.objects("Auth").length > 0;
      store.dispatch(initAuth(hasPasscode));
      setLoading(false);
    })();
  }, []);

  if (loading) return <Loading>Some sort of loading</Loading>;
  else
    return (
      <Router>
        {isLocked ? (
          hasPasscode ? (
            <UnlockPage />
          ) : (
            <ChangePinPage />
          )
        ) : (
          <Routes>
            <Route element={<HomePage />} path="/" />
            <Route element={<PasswordsPage />} path="passwords" />
            <Route element={<SettingsPage />} path="settings" />
          </Routes>
        )}
      </Router>
    );
}
