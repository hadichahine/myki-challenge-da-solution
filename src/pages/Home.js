import React from "react";
import PanelLayout from "../layouts/PanelLayout";

export default function HomePage() {
  return <PanelLayout />;
}
