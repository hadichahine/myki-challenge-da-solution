import os from "os";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

const BackNavLink = styled(NavLink)`
  color: white;
`;

const SystemInfo = styled.div`
  padding: 20px;
`;

const SystemInfoTitle = styled.h1`
  color: white;
`;

const SystemInfoEntryStyle = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  color: white;

  & > h2 {
    font-size: 16px;
    margin: 0;
  }

  & > span {
    font-size: 16px;
  }
`;

function SystemInfoEntry({ label, value }) {
  return (
    <SystemInfoEntryStyle>
      <h2>{label}</h2>
      <div>{value}</div>
    </SystemInfoEntryStyle>
  );
}

export default function SettingsPage() {
  return (
    <SystemInfo>
      <BackNavLink to="/">Back</BackNavLink>
      <SystemInfoTitle>System Info</SystemInfoTitle>
      <SystemInfoEntry
        label="Platform"
        value={`${os.type()} - ${os.hostname()}`}
      />
    </SystemInfo>
  );
}
