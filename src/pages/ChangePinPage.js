import { useState } from "react";
import PinInput from "../components/PinInput";
import updatePin from "../usecases/updatePin";

export default function ChangePinPage() {
  const [pin, setPin] = useState("");

  function onCompleteChange(confirmPin) {
    if (confirmPin === pin) updatePin({ pin });
  }

  return pin ? (
    <PinInput
      key={"confirm"}
      label="Confirm your pin"
      max={6}
      onComplete={onCompleteChange}
    />
  ) : (
    <PinInput key={"set"} label="Set your pin" max={6} onComplete={setPin} />
  );
}
