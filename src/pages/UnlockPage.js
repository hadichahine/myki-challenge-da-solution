import { useState } from "react";
import PinInput from "../components/PinInput";
import unlock from "../usecases/unlock";

export default function UnlockPage() {
  const [lockError, setLockError] = useState(false);

  return (
    <PinInput
      error={lockError}
      label="Write pin to unlock"
      max={6}
      onComplete={(pin) => {
        try {
          unlock({ pin });
        } catch (exception) {
          setLockError(true);
        }
      }}
    />
  );
}
