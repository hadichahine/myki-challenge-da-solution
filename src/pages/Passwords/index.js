import React from "react";
import { useSelector } from "react-redux";
import PanelLayout from "../../layouts/PanelLayout";
import PasswordForm from "./form";
import styled, { css } from "styled-components";
import ListAndViewLayout from "../../layouts/ListAndViewLayout";

const ListItem = styled.div`
  color: white;
  display: flex;
  flex-direction: column;
  gap: 5px;
  background-color: ${({ chosen }) => (chosen ? "#0c5552" : css`#21262a`)};
  padding: 4px 10px;
  font-weight: normal;
  cursor: pointer;

  & > * {
    padding: 0;
    margin: 0;
  }
`;

function StandardListItem({ host, nickname, view, chosen }) {
  return (
    <ListItem onClick={view} chosen={chosen}>
      <h4>{host}</h4>
      <span>{nickname}</span>
    </ListItem>
  );
}

export default function PasswordsPage() {
  const passwords = useSelector((state) => state.passwords);

  return (
    <PanelLayout>
      <ListAndViewLayout
        items={passwords}
        ListItem={StandardListItem}
        ListItemView={PasswordForm}
      />
    </PanelLayout>
  );
}
