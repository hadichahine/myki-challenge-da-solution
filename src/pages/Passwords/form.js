import { useMemo, useState } from "react";
import styled from "styled-components";
import randomstring from "randomstring";
import addPassword from "../../usecases/addPassword";

const Form = styled.div`
  display: flex;
  flex-direction: column;
`;

const FormInputText = styled.input`
  border: 0;
  outline: none;
  padding: 15px;
  color: white;
  background: #14181c;
  border-radius: 2px;
`;

const PasswordGenerator = styled.div`
  background: #109b87;
`;

export default function PasswordFormFragment({
  state = "view",
  value,
  onAction,
}) {
  const [host, setHost] = useState(value?.host);
  const [username, setUsername] = useState(value?.username);
  const [password, setPassword] = useState(value?.password);
  const [nickname, setNickname] = useState(value?.nickname);

  const [isGeneratorShown, setGeneatorShown] = useState(false);

  const [range, setRange] = useState(16);

  const generatedPassword = useMemo(
    () =>
      randomstring.generate({
        length: range,
      }),
    [range]
  );

  return (
    <div>
      <div>
        {state === "create" && (
          <button
            onClick={() => {
              const entry = { host, username, password, nickname };
              addPassword(entry);
              onAction(entry);
            }}
          >
            Create
          </button>
        )}
      </div>
      <Form>
        <FormInputText
          value={host}
          placeholder="Host"
          onChange={({ target: { value } }) => setHost(value)}
        />
        <FormInputText
          value={nickname}
          placeholder="Nickname"
          onChange={({ target: { value } }) => setNickname(value)}
        />
        <FormInputText
          value={username}
          placeholder="Username"
          onChange={({ target: { value } }) => setUsername(value)}
        />
        <div
          style={{
            display: "flex",
            background: "#14181c",
          }}
        >
          <FormInputText
            style={{
              flex: 1,
            }}
            value={password}
            placeholder="Password"
            onChange={({ target: { value } }) => setPassword(value)}
          />
          {state === "create" && (
            <div
              style={{
                color: "white",
                cursor: "pointer",
              }}
              onClick={() => {
                setGeneatorShown(!isGeneratorShown);
              }}
            >
              {isGeneratorShown ? "Close" : "Generate"}
            </div>
          )}
        </div>
        {isGeneratorShown && (
          <PasswordGenerator>
            <span>Generate Password</span>
            <div>
              <input
                onChange={({ target: { value } }) => setRange(value)}
                type="range"
                value={range}
                min="1"
                max="26"
              ></input>
            </div>
            <div>{generatedPassword}</div>
            <button onClick={() => setPassword(generatedPassword)}>
              Choose
            </button>
          </PasswordGenerator>
        )}
      </Form>
    </div>
  );
}
