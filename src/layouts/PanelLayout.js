import { NavLink, useLocation } from "react-router-dom";
import styled, { css } from "styled-components";

const items = [
  {
    name: "Passwords",
    path: "/passwords",
  },
  {
    name: "Settings",
    path: "/settings",
  },
];

const Panel = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  gap: 1px;
  grid-template-columns: 180px 1fr;
  grid-template-rows: 1fr;
`;

const SidePane = styled.div`
  background: #21262a;
`;

const SidePaneTitle = styled.h2`
  color: grey;
  font-size: 18px;
  padding: 10px;
  margin: 0;
`;

const NavList = styled.div`
  display: flex;
  flex-direction: column;
`;

const NavItem = styled(NavLink)`
  background: ${({ chosen }) => (chosen ? css`#363a40` : css`transparent`)};
  padding: 10px;
  color: white;
  cursor: pointer;
  color: white;
  text-decoration: none;
`;

export default function PanelLayout({ children }) {
  const location = useLocation();

  return (
    <Panel>
      <SidePane>
        <SidePaneTitle>Items</SidePaneTitle>
        <NavList>
          {items.map(({ name, path }) => (
            <NavItem to={path} chosen={location.pathname === path}>
              {name}
            </NavItem>
          ))}
        </NavList>
      </SidePane>
      {children}
    </Panel>
  );
}
