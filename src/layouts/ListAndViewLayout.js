import React, { useState } from "react";
import styled from "styled-components";

const ListViewLayout = styled.div`
  display: grid;
  gap: 1px;
  grid-template-columns: 220px 1fr;
  grid-template-rows: 1fr;
`;

const List = styled.div`
  background: #14181c;
  display: flex;
  flex-direction: column;
`;

export default function ListAndViewLayout({ items, ListItem, ListItemView }) {
  const [chosenIndex, setChosenIndex] = useState(null);

  const [open, setOpen] = useState(false);

  const chosen = items[chosenIndex];

  const state = open ? "create" : "view";

  function view(index) {
    setChosenIndex(index);
    setOpen(false);
  }

  function isChosen(index) {
    return index === chosenIndex;
  }

  function create() {
    setChosenIndex(null);
    setOpen(true);
  }

  function onAction(entry, { setOpen }) {
    setOpen(false);
  }

  return (
    <ListViewLayout>
      <div>
        <div>
          <button onClick={create}>Add</button>
        </div>
        <List>
          {items.map((entry, index) => (
            <ListItem
              key={index}
              {...entry}
              view={() => view(index)}
              chosen={isChosen(index)}
            />
          ))}
        </List>
      </div>
      {(open || chosenIndex !== null) && (
        <ListItemView
          key={chosenIndex}
          state={state}
          value={chosen}
          onAction={(entry) => onAction(entry, { setOpen })}
        />
      )}
    </ListViewLayout>
  );
}
