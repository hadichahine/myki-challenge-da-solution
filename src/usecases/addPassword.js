import { getDB } from "../database";
import { addPasswordAction } from "../appstate/passwords";
import { encryptDataAES } from "../utils/crypto";
import store from "../appstate/store";

export default function addPassword({ username, password, host, nickname }) {
  const encryptionKey = store.getState().auth.encryptionKey;

  const db = getDB();

  const { result: encryptedUsername, initVector: initVectorUsername } =
    encryptDataAES(username, encryptionKey, "utf-8");

  const { result: encryptedPassword, initVector: initVectorPassword } =
    encryptDataAES(password, encryptionKey, "utf-8");

  db.write(() => {
    db.create("Password", {
      encryptedUsername,
      encryptedPassword,
      initVectorUsername,
      initVectorPassword,
      host,
      nickname,
    });
  });

  store.dispatch(
    addPasswordAction({
      nickname,
      username,
      password,
      host,
    })
  );
}
