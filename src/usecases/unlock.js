import { decryptDataAES, saltkdf } from "../utils/crypto";
import store from "../appstate/store";
import { getDB } from "../database";
import { unlock as unlockAction } from "../appstate/auth";
import { loadInitialPasswords } from "../appstate/passwords";
import getPasswordRecords from "./getPasswordsRecords";

export default function unlock({ pin }) {
  const db = getDB();

  const auth = db.objects("Auth")[0];

  const encryptionKey = decryptDataAES(
    auth.encryptedEncryptionKey,
    saltkdf(pin, 32, auth.salt).key,
    auth.initVector
  );

  store.dispatch(
    unlockAction({
      encryptionKey,
    })
  );

  const passwords = getPasswordRecords();

  store.dispatch(loadInitialPasswords({ passwords }));
}
