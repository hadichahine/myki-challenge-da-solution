import { encryptDataAES, saltkdf } from "../utils/crypto";
import store from "../appstate/store";
import crypto from "crypto";
import { unlock } from "../appstate/auth";
import { getDB } from "../database";

export default function updatePin({ pin }) {
  const encryptionKey = crypto.randomBytes(32).toString("hex");

  const { salt, key } = saltkdf(pin, 32);

  let { initVector, result: encryptedEncryptionKey } = encryptDataAES(
    encryptionKey,
    key
  );

  const db = getDB();

  db.write(() => {
    db.create("Auth", {
      salt,
      initVector,
      encryptedEncryptionKey,
    });
  });

  store.dispatch(
    unlock({
      encryptionKey,
    })
  );
}
