import { getDB } from "../database";
import { decryptDataAES } from "../utils/crypto";
import store from "../appstate/store";

export default function getPasswordRecords() {
  const encryptionKey = store.getState().auth.encryptionKey;

  const db = getDB();

  return db
    .objects("Password")
    .map(
      ({
        _id,
        host,
        nickname,
        encryptedUsername,
        encryptedPassword,
        initVectorUsername,
        initVectorPassword,
      }) => ({
        id: _id,
        host,
        nickname,
        username: decryptDataAES(
          encryptedUsername,
          encryptionKey,
          initVectorUsername,
          "utf-8"
        ),
        password: decryptDataAES(
          encryptedPassword,
          encryptionKey,
          initVectorPassword,
          "utf-8"
        ),
      })
    );
}
