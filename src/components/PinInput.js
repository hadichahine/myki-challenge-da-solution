import React, { useEffect, useState } from "react";

export default function PinInput({
  label,
  error,
  max = 6,
  onComplete = () => {},
}) {
  const [pin, setPin] = useState("");

  const digits = pin.split("");

  useEffect(() => {
    if (pin.length === max) onComplete(pin);
  }, [pin, max, onComplete]);

  function onChangePinInput({ target: { value } }) {
    if (value.length <= max) setPin(value);
  }

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        gap: 15,
        margin: 0,
        color: "white",
        fontSize: "20px",
      }}
    >
      <span>{label}</span>
      <input
        autoFocus
        type="number"
        value={pin}
        style={{
          height: 0,
          width: 0,
          border: "none",
          padding: 0,
        }}
        onChange={onChangePinInput}
      />
      <div style={{ display: "flex", gap: "0 10px" }}>
        {[...digits, ...new Array(max - digits.length)].map((digit, index) => (
          <div
            style={{
              backgroundColor: "red",
              width: "60px",
              height: "60px",
              lineHeight: "100%",
              textAlign: "center",
              fontSize: 40,
              background:
                digits.length === index
                  ? `#${error ? "ff0000" : "ffffff"}3f`
                  : `#${error ? "ff0000" : "ffffff"}2a`,
              borderRadius: "5%",
              verticalAlign: "middle",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            key={index}
          >
            {digit ? (
              <div
                style={{
                  backgroundColor: "white",
                  width: "15%",
                  height: "15%",
                  borderRadius: "100%",
                }}
              />
            ) : (
              ""
            )}
          </div>
        ))}
      </div>
    </div>
  );
}
