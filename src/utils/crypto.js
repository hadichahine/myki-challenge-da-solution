import crypto from "crypto";

export function encryptDataAES(data, key, encoding = "hex") {
  const initVector = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv(
    "aes-256-cbc",
    Uint8Array.from(Buffer.from(key, "hex")),
    initVector
  );

  let result = cipher.update(data, encoding, "hex");

  result += cipher.final("hex");

  return {
    initVector: Buffer.from(initVector).toString("hex"),
    result,
  };
}

export function decryptDataAES(
  encryptedData,
  key,
  initVector,
  encoding = "hex"
) {
  const decipher = crypto.createDecipheriv(
    "aes-256-cbc",
    Uint8Array.from(Buffer.from(key, "hex")),
    Uint8Array.from(Buffer.from(initVector, "hex"))
  );

  let decryptedData = decipher.update(encryptedData, "hex", encoding);

  decryptedData += decipher.final(encoding);

  return decryptedData;
}

export function sha256(data) {
  return crypto.createHash("sha256").update(data).digest("hex");
}

export function saltkdf(mainKey, size, givenSalt) {
  const textEncoder = new TextEncoder("utf-8");

  const salt = givenSalt
    ? Uint8Array.from(Buffer.from(givenSalt, "hex"))
    : crypto.randomBytes(size - mainKey.length);

  return {
    salt: Buffer.from(salt).toString("hex"),
    key: sha256(new Uint8Array([...textEncoder.encode(mainKey), ...salt])),
  };
}
