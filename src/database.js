import Realm from "realm";

const Auth = {
  name: "Auth",
  properties: {
    salt: "string",
    initVector: "string",
    encryptedEncryptionKey: "string",
  },
};

const Password = {
  name: "Password",
  properties: {
    encryptedUsername: "string",
    encryptedPassword: "string",
    initVectorUsername: "string",
    initVectorPassword: "string",
    host: "string",
    nickname: "string",
  },
};

let db;

export async function loadDB() {
  db = await Realm.open({
    schema: [Auth, Password],
    path: "./db/db",
  });
}

export function getDB() {
  return db;
}
